# adapted from https://github.com/STAT545-UBC/STAT545-UBC.github.io

all: talk.pdf talk.html

# Patterns

%.pdf: %.Rnw
	Rscript -e 'knitr::knit2pdf("$<")'

%.html: %.Rmd
	Rscript -e 'rmarkdown::render("$<")'

